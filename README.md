# Maps under the Loire river - GitLab Pages

During the 30th International Conference on the History of Cartography [ICHC 2024](https://ichc2024.univ-lyon3.fr/), we presented a scientific poster about the *Gorges de la Loire* representation in several types of historical maps, with different scales.

On this GitLab, we aims to present materials, ressources and more maps. That inclued :

* The [scientific poster](https://pomazagol.gitpages.huma-num.fr/maps-under-the-loire-river/poster.html) itself;
* [A list of serveral 16th, 17th, 18th centuries maps](https://pomazagol.gitpages.huma-num.fr/maps-under-the-loire-river/maps_collection.html),
* [A GIS web application](https://pomazagol.gitpages.huma-num.fr/maps-under-the-loire-river/gis_web_app.html) allowing to navigate in some of these maps, and other modern ones.
* [Ressources on mapping the *Gorges de la Loire* heritage](https://pomazagol.gitpages.huma-num.fr/maps-under-the-loire-river/grangent_sunken_landscape.html)

This GitLab is not set in stone and all these ressources will be completed.

# A scientific poster

  This poster results of a collaboration between 2 members of the [EVS lab](https://umr5600.cnrs.fr/) (UMR 5600 CNRS) from the [Univerty of Saint-Étienne](https://www.univ-st-etienne.fr/): a GIS engineer (Dr Pierre-Olivier Mazagol) who has made a tool to see ancient landscapes of the Gorge de la Loire, a little segment of the Loire River near Saint-Étienne agglomeration, and a geographer (Dr Sarah Réault) specialised in geo-history. Both are 
  
  Those maps extracts are organised about 2 orientations:
  * Mapping development between 15th century and current methods for cartography.
  * Trying to understand how possibilities to cross and follow the river (sailing or using banks). All kinds of movements facilitated remotely by reading a map, in peacetime or wartime. That is the reason we choose as title "The *Gorges de la Loire* as a strategic mapped area" for this poster.

  Its structure focuses of 3 subjects: 
  
  On either side of a current map, extracts are dated from 15th to 19th century:
   * On left side: The question of the representation of river as an obstacle, possibly political limit.
   * On right side: crossing and sailing.

Below, the poster give priority to mapping in war or cold war context: maps are from begin of 19th century to middle of 20th century.
This overview ends with new methods of representation, between, map and landscape.
You can use the qr-code to visit mores resources.
        
Contact : <a href= "mailto:pierre.olivier.mazagol@univ-st-etienne.fr">pierre.olivier.mazagol[a]univ-st-etienne.fr</a> and <a href= "mailto:pierre.olivier.mazagol@univ-st-etienne.fr">sarah.reault[a]univ-st-etienne.fr</a>
      </p>


<figure>
<img src="public/images/Carte_2_Grangent_XVIIe_Carte_Lionnois_Forest_extrait.jpg" alt="Carte du Lionnois, Forest, Beaujolois et Masconnois (Jean Le Clerc excudit, 1619)" width="500"/>
  <figcaption>Carte du Lionnois, Forest, Beaujolois et Masconnois (Jean Le Clerc excudit, 1619)</figcaption>
</figure>

